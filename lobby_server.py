#!/usr/bin/env python3
import sys
if sys.version_info < (3,5):
    sys.stderr.write("Minimal python verson not matched\n")
    exit(1)

from websocket_server import WebsocketServer
import json

PORT=9001

games_list = []

# Called for every client connecting (after handshake)
def new_client(client, server):
    print("New client connected and was given id %d" % client['id'])
    server.send_message(client, json.dumps({
        "command": "games_list",
        "games_list": games_list,
    }))
    #server.send_message_to_all("Hey all, a new client has joined us")



def update_game_name(data):
    global games_list
    client_id = data['id']
    del data['command']
    games_list = [x for x in games_list if x['id'] != client_id]
    games_list.append(data)

def remove_listing(data):
    global games_list
    client_id = data['client_id']
    games_list = [x for x in games_list if x['client_id'] != client_id]
    #games_list = filter(lambda x: x['client_id'] != client_id, games_list)

def invalid_command(data):
    print("invalid_command", data['command'])
    pass

# Called for every client disconnecting
def client_left(client, server):
    global games_list
    print("Client(%d) disconnected" % client['id'])
    client_id = client['id']
    games = [x for x in games_list if x['client_id'] == client_id]
    for game in games:
        data = {
            "command": "remove_listing",
            "client_id": client['id'],
            "id": game['id'],
        }
        remove_listing(data)
        server.send_message_to_all(json.dumps(data))

# Called when a client sends a message
def message_received(client, server, message):
    print("Client(%d) said: %s" % (client['id'], message[:66]))
    data = json.loads(message)
    data['client_id'] = client['id']
    {
        "update_game_name": update_game_name,
        "remove_listing": remove_listing,
    }.get(data['command'], invalid_command)(data)
    server.send_message_to_all(message)

    #for peer in server.clients:
    #    if client['id'] != peer['id']:
    #        peer.send_message(message)




if __name__ == '__main__':
    server = WebsocketServer(PORT)
    server.set_fn_new_client(new_client)
    server.set_fn_client_left(client_left)
    server.set_fn_message_received(message_received)
    server.run_forever()
