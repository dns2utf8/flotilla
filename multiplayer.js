var Multiplayer = (function() {
    'use strict'
    const log = console.log;
    const lb_offline = Symbol('lb_offline');
    const lb_connecting = Symbol('lb_connecting');
    const lb_conncted = Symbol('lb_conncted');
    const multiplayer_config = document.querySelector('#multiplayer_config');
    const multiplayer_game_name = multiplayer_config.querySelector('#multiplayer_game_name');
    const multiplayer_available_games = multiplayer_config.querySelector('#multiplayer_available_games');

    let ws = undefined;
    let pc = undefined;
    let dc = undefined;
    let status = lb_offline;
    const my_id = round(random() * pow(2, 32));
    let player_config = [];
    let global_games_list = [];

    multiplayer_game_name.addEventListener('keyup', ev => {
        ev.preventDefault();
        if (
            ev.keyCode === 13 ||
            (ev.keyCode >= 65 && ev.keyCode <= 90) ||
            (ev.keyCode >= 48 && ev.keyCode <= 57)
        ) {
            update_game_name();
        }
    })
    multiplayer_game_name.addEventListener('change', _ => update_game_name())

    multiplayer_available_games.addEventListener('change', _ => update_game_selection())

    init();

    /// Lobby Server is a dumb broadcaster
    function init() {
        try {
            ws = new WebSocket(MP_WS_LOBBY_SERVER);

            ws.onopen = event => {
                console.log(["ws.onopen", event])

                update_status(lb_conncted)
                update_game_name();
            }
            ws.onmessage = event => {
                console.log(['ws.onmessage', event.data]);
                if (event.data === 'PING') { return }
                const data = JSON.parse(event.data);
                handle_incoming(data);
            }
            ws.onclose = event => {
                console.log(['ws.onmessage', event])
                update_status(lb_offline)
            }
            ws.onerror = event => {
                console.error(['ws.onerror', event])
                update_status(lb_offline)
            }
        } catch(e) {
            console.error(['ws.onerror', e])
            update_status(lb_offline)
        }
    }

    return {
        ws: _ => ws,
        pc: _ => pc,
        dc: _ => dc,
        status: _ => status,
        update_player_config: l => {
            player_config = l;
            update_game_name();
        },
        setup_peers: setup_peers,
    }

    function update_status(new_status) {
        const info = multiplayer_config.querySelector('#multiplayer_status .info');
        const loader = multiplayer_config.querySelector('#multiplayer_status .loader');
        switch (new_status) {
            case lb_offline:
                loader.classList.add('hidden');
                info.innerHTML = 'Offline <button>Retry</button>'
                info.querySelector('button').addEventListener('click', ev => {
                    ev.preventDefault();
                    init();
                })
                break;
            case lb_connecting:
                loader.classList.remove('hidden');
                info.innerHTML = 'Connecting to Lobby Server ...'
                break;
            case lb_conncted:
                loader.classList.add('hidden');
                info.innerHTML = `✔ Lobby (connected as ${my_id})`
                break;
            default:
                console.error(['update_status.unhandled', new_status]);
        }
        status = new_status;
    }

    /// Assert ws is connected
    function update_game_name() {
        if (status !== lb_conncted) {
            console.error(['update_game_name not connected', status]);
            return;
        }
        const name = multiplayer_game_name.value.trim();
        if (name === '') {
            multiplayer_game_name.focus();
        } else {
            if (player_config.length !== 0) {
                ws.send(JSON.stringify({
                    command: 'update_game_name',
                    id: my_id,
                    game_name: name,
                    player_config: player_config,
                }));
            } else {
                ws.send(JSON.stringify({
                    command: 'remove_listing',
                    id: my_id,
                    game_name: name,
                }));
            }
        }
    }

    function handle_incoming(data) {
        switch (data['command']) {
            case 'games_list':
                global_games_list = data['games_list'];
                break;
            case 'update_game_name':
                global_games_list = global_games_list.filter(x => x.id != data['id'])
                global_games_list.push(data)
                break;
            case 'remove_listing':
                global_games_list = global_games_list.filter(x => x.id != data['id'])
                break;
            default:
                console.error(['handle_incoming invalid command', data['command']]);
                return;
        }

        const name = multiplayer_game_name.value.trim();
        let html = '';
        global_games_list.forEach(g => {
            const selected = name === g.game_name ? 'selected="selected"' : '';
            html += `<option value="${g.id}" ${selected}>${g.game_name}</option>`
        })
        multiplayer_available_games.innerHTML = html;
        multiplayer_available_games.size = Math.min(12, Math.max(2, global_games_list.length));
    }

    function update_game_selection() {
        const game_id = multiplayer_available_games.value;
        console.log(['switch to', game_id])
    }

    function sendToServer(object) {
        //if (status !== lb_conncted) { return; }
        console.log(['sendToServer', object])
        object['command'] = 'broadcast'
        ws.send(JSON.stringify(object));
    }
    /*************************** The P2P code **************************************/

    /// hints: https://github.com/mdn/samples-server/blob/master/s/webrtc-from-chat/chatclient.js
    function setup_peers() {
        pc = new RTCPeerConnection({
            iceServers: [     // Information about ICE servers - Use your own!
                /*{
                    urls: "turn:" + myHostname,  // A TURN server
                    username: "webrtc",
                    credential: "turnserver"
                },*/
                {
                    urls: [
                        'stun:stun.services.mozilla.com',
                        'stun:stun.l.google.com:19302',
                    ]
                },
            ]
        })

        pc.ondatachannel = event => {
            console.log(['Multiplayer.ondatachannel', event])
        };

        dc = pc.createDataChannel('some random named channel');
        dc.onmessage = function (event) {
            console.log("received: " + event.data);
        };

        dc.onopen = function () {
            console.log("datachannel open");
            dc.send([42, "test"])
        };

        dc.onclose = function () {
            console.log("datachannel close");
        };

        pc.onnegotiationneeded = handleNegotiationNeededEvent;
        pc.onicecandidate = handleICECandidateEvent;
        pc.onnremovestream = console.log // handleRemoveStreamEvent;
        pc.oniceconnectionstatechange = console.log // handleICEConnectionStateChangeEvent;
        pc.onicegatheringstatechange = console.log // handleICEGatheringStateChangeEvent;
        pc.onsignalingstatechange = console.log // handleSignalingStateChangeEvent;
    }

    // Called by the WebRTC layer to let us know when it's time to
    // begin (or restart) ICE negotiation. Starts by creating a WebRTC
    // offer, then sets it as the description of our local media
    // (which configures our local media stream), then sends the
    // description to the callee as an offer. This is a proposed media
    // format, codec, resolution, etc.
    function handleNegotiationNeededEvent() {
        log("*** Negotiation needed");

        log("---> Creating offer");
        pc.createOffer().then(function(offer) {
            log("---> Creating new description object to send to remote peer");
            return pc.setLocalDescription(offer);
        })
        .then(function() {
            log("---> Sending offer to remote peer");
            sendToServer({
                name: my_id,
                type: "video-offer",
                sdp: pc.localDescription
            });
        })
        .catch(reportError);
    }


    // Handles |icecandidate| events by forwarding the specified
    // ICE candidate (created by our local ICE agent) to the other
    // peer through the signaling server.
    function handleICECandidateEvent(event) {
        if (event.candidate) {
            log("Outgoing ICE candidate: " + event.candidate.candidate);

            sendToServer({
                type: "new-ice-candidate",
                target: 0,
                candidate: event.candidate
            });
        }
    }
})();
