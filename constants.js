"use strict"
// Natural
const PI = Math.PI
    , PI2 = 2 * Math.PI
    , sin = Math.sin
    , cos = Math.cos
    , asin = Math.asin
    , acos = Math.acos
    , sqrt = Math.sqrt
    , pow = Math.pow
    , abs = Math.abs
    , atan = Math.atan
    , atan2 = Math.atan2
    , sign = Math.sign
    , random = Math.random
    , round = Math.round

// Game
const DEBUG_LEVEL = 0;
const BASE_SHIP_RADIUS = 200;
const PLAYER_COLORS = [
    '#ff5e00',
    '#4533de',
    '#ff33ef',
    '#2dc32d',
    '#10b7ce',
];
const max_fps = 1000 / 20;
const MAX_PLAYERS = 6;
const MP_WS_LOBBY_SERVER = 'ws://localhost:9001/';

// Utils
var global_unique_id_oracle = 0;
function get_unique_id() {
    return global_unique_id_oracle++;
}

// calculate the dot product of two vectors
function dot_prod_angle(x0, y0, x1, y1) {
    const len0 = sqrt(pow(x0, 2) + pow(y0, 2));
    const len1 = sqrt(pow(x1, 2) + pow(y1, 2));
    const sum = x0 * x1 + y0 * y1;

    return acos(sum / (len0 * len1));
}
function norm(x, y) {
    const len = sqrt(pow(x, 2) + pow(y, 2));
    return [x / len, y / len];
}
function delta_atan2(x0, y0, x1, y1) {
    const d0 = x1 - x0;
    const d1 = y1 - y0;
    return atan2(d1, d0)
}
