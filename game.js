function Game(ctx, n_players) {
    this.ctx = ctx;
    this.n_players = n_players;
    this.input_command_queue = [];
    this.objects = [];
    this.last_target_reset = 0;

    this.init(n_players);
}

+function() {
    "use strict"

    const MAX_TARGET_KEEP_TIME = 100;

    Game.prototype = {
        'init': function(n_players) {
            const winkel_anteil = PI2 / n_players;
            
            for (var i = 0; i < n_players; ++i) {
                let winkel = winkel_anteil * i;
                let x = -sin(winkel) * BASE_SHIP_RADIUS;
                let y = cos(winkel) * BASE_SHIP_RADIUS;

                // TODO warum ist der Winkel so korrekt ...
                //let base = new Base(i, x, y, (winkel_anteil * (n_players - i)) - PI2 / 4)
                let base = new Base(i, x, y, winkel)
                this.objects.push(base);
            }
        },
        'render': function(resized) {
            let ctx = this.ctx;
            //ctx.save();
            ctx.setTransform(1, 0, 0, 1, 0, 0);
            let canvas = this.ctx.canvas;
            if (canvas.height !== canvas.offsetHeight) {
                resized = true;
            }
            if (resized) {
                var width = canvas.width = canvas.offsetWidth;
                var height = canvas.height = canvas.offsetHeight;
                try {
                    ctx.imageSmoothingEnabled = true;
                } catch (error) {
                    // ignore
                }
            } else {
                var width = canvas.width;
                var height = canvas.height;
            }
            ctx.clearRect(0, 0, width, height);
            //ctx.translate(width / 2, height / 2);
            ctx.transform(1,0,0,1, width / 2, height / 2);

            // background and markings
            ctx.strokeStyle = 'rgba(255,255,255, 0.3)';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.arc(0, 0, BASE_SHIP_RADIUS, 0, PI2);
            ctx.stroke();
            for (let i = 0; i < 4; ++i) {
                ctx.beginPath();
                ctx.arc(0, 0, BASE_SHIP_RADIUS + 5, i*(PI2/4), i*(PI2/4) + PI2/5);
                ctx.stroke();
            }

            this.last_target_reset += 1;
            if (this.last_target_reset > MAX_TARGET_KEEP_TIME) {
                this.last_target_reset = 0;
                var reset_targets = true;
                console.log('resetting targets')
            } else {
                var reset_targets = false;
            }

            // handle objects
            this.objects.forEach(o => {
                o.update(); // TODO delta_t
                o.render(ctx);

                if (reset_targets) {
                    o.target_id = undefined;
                }

                // Collision Detection
                this.handle_collisions(o);

                if (abs(o.x) > 1000 || abs(o.y) > 1000) {
                    console.error(['runaway object', o])
                    o.x = o.y = o.x_next = o.y_next = 0;
                    o.tail.reset();
                }
            });
            this.objects = this.objects.filter(o => o.current_hit_points > 0);
            var mothership_count = 0;
            this.objects.forEach(o => {
                if (o instanceof Base) {
                    mothership_count += 1;
                }
            });
            if (mothership_count === 1 && this.on_game_over) {
                this.on_game_over()
            }
            this.input_command_queue.forEach(c => {
                const mothership = this.objects.filter(o => o.player === c.player)[0];
                if (mothership instanceof Base === false) {
                    return; // mothership has already died
                }
                if (c.command === 'do_construction') {
                    this.objects.push(new Fighter(mothership.player, mothership.x +0, mothership.y +0, mothership.orientation +0, this))
                }
            });
            this.input_command_queue = [];
            //ctx.fillColor = '#f00'
            //ctx.fillRect(20, 30, width - 40, height - 60);

            //ctx.restore();
        },
        'submit_player_input': function(input) {
            this.input_command_queue.push(input);
        },
        'handle_collisions': function(current_object) {
            if (current_object.current_hit_points < 0) {
                return; // ignore dead
            }
            this.objects.forEach(o => {
                if (o.uid === current_object.uid || o.current_hit_points < 0) {
                    return; // filter self or dead
                }
                const dist2 = pow(o.x - current_object.x, 2) + pow(o.y - current_object.y, 2);
                if (dist2 > 128) {
                    return; // too far away
                }
                if (current_object.current_hit_points > o.current_hit_points) {
                    var heavy = current_object;
                    var light = o;
                } else {
                    var heavy = o;
                    var light = current_object;
                }
                if (o.player === current_object.player) {
                    // repell without damage
                    light.x_next += sign(light.x - heavy.x) * 0.1;
                    light.y_next += sign(light.y - heavy.y) * 0.1;
                } else {
                    // make damage
                    heavy.current_hit_points -= light.get_damage();
                    light.current_hit_points -= heavy.get_damage();
                }
            })
        },
    }
}()
