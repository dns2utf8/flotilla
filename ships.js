/// Homebase of the player
function Base(player, x, y, orientation) {
    this.uid = get_unique_id();
    this.player = player;
    this.x = x;
    this.y = y;
    if (orientation < 0) { orientation += PI2; }
    this.orientation = orientation;
    
    this.current_hit_points = 100;
}

/// First fighter class
function Fighter(player, x, y, orientation, world) {
    this.uid = get_unique_id();
    this.current_hit_points = 5;
    this.player = player;
    this.x_next = this.x = x;
    this.y_next = this.y = y;
    this.orientation = orientation;
    this.world = world;

    this.gas = 1.0;
    this.target = undefined;
    this.autopilot_target_id = player;

    this.tail = new RingBuf(256);

    this.update();
}

+function() {
    "use strict"

    const BASE_MOVE_SPEED = 0.5;
    const FIGHTER_MOVE_SPEED = 5;

    Base.prototype = {
        'render': function(ctx) {
            ctx.save();


            // BROKEN: ctx.translate(this.x, this.y);
            ctx.transform(1,0,0,1, this.x, this.y); // set scale and position
            ctx.save();

            ctx.rotate(this.orientation);
            // draw ship
            ctx.fillStyle = PLAYER_COLORS[this.player];
            ctx.strokeStyle = 'rgba(128,128,128, 0.6)';

            ctx.beginPath();
            ctx.moveTo(15, 0);

            ctx.lineTo(5, -15);
            ctx.lineTo(0, -10);
            ctx.lineTo(-5, -15);
            ctx.lineTo(-18, -20);

            ctx.lineTo(-12, -3);
            ctx.lineTo(-10, 0);
            ctx.lineTo(-12, 3);

            ctx.lineTo(-18, 20);
            ctx.lineTo(-5, 15);
            ctx.lineTo(0, 10);
            ctx.lineTo(5, 15);
            ctx.fill();
            ctx.stroke();

            ctx.restore();

            // draw health
            ctx.fillStyle = 'rgba(255,255,200, 0.7)';
            ctx.strokeStyle = 'rgba(255,255,255, 0.5)';
            ctx.lineWidth = 5;
            ctx.beginPath();
            ctx.arc(0, 0, 8, 0, PI2);
            ctx.fill();
            ctx.stroke();
            if (this.current_hit_points < 15) {
                ctx.strokeStyle = 'rgba(255,0,0, 0.7)';
            } else if (this.current_hit_points < 60) {
                ctx.strokeStyle = 'rgba(255,255,0, 0.7)';
            } else {
                ctx.strokeStyle = 'rgba(0,255,0, 0.7)';
            }
            ctx.beginPath();
            ctx.arc(0, 0, 8, 0, this.current_hit_points / 100 * PI2);
            ctx.stroke();

            ctx.restore();

            if (DEBUG_LEVEL > 1) {
                ctx.fillText(("h: "+this.current_hit_points).substring(0, 8), this.x + 25, this.y - 5);
                ctx.fillText(("o: "+this.orientation).substring(0, 8), this.x + 25, this.y + 5);
            }
        },
        'update': function() {
            this.x += cos(this.orientation) * 0.1;
            this.y += sin(this.orientation) * 0.1;

            // keep fields updated for autopilot
            // TODO implement park position with offset
            this.x_next = this.x;
            this.y_next = this.y;
            
            const dist_center2 = pow(this.x, 2) + pow(this.y, 2);
            if (dist_center2 - BASE_SHIP_RADIUS*BASE_SHIP_RADIUS > 42) {
                // FIXME magic number
                this.orientation -= 0.001;
            }

            if (this.orientation > PI2) {
                this.orientation -= PI2;
            }
            if (this.orientation < 0) {
                this.orientation += PI2;
            }
        },
        'get_damage': function() {
            return 100;
        },
    };

    Fighter.prototype = {
        'render': function(ctx) {
            ctx.save();


            // Debug
            if (DEBUG_LEVEL > 0 && this.autopilot_target_id !== undefined) {
                ctx.strokeStyle = 'rgba(255,255,120, 0.8)';
                ctx.lineWidth = 2;
                ctx.beginPath();
                ctx.moveTo(this.x, this.y);
                const target_x = this.world.objects[this.autopilot_target_id].x_next
                , target_y = this.world.objects[this.autopilot_target_id].y_next; 
                ctx.lineTo(target_x, target_y);
                ctx.stroke();
                
                const tail_beginning = this.tail.first();
                if (tail_beginning !== undefined) {
                    ctx.strokeStyle = 'rgba(255,120,120, 0.8)';
                    ctx.lineWidth = 1;
                    ctx.beginPath();
                    ctx.moveTo(tail_beginning.x, tail_beginning.y);
                    this.tail.forEach(p => ctx.lineTo(p.x, p.y));
                    ctx.stroke();
                }
            }

            ctx.strokeStyle = 'rgba(255,255,255, 0.8)';
            ctx.lineWidth = 1;
            ctx.fillStyle = PLAYER_COLORS[this.player];

            const scale = 1.3;
            ctx.transform(scale,0,0,scale, this.x, this.y); // set scale and position
            ctx.rotate(this.orientation);

            ctx.beginPath();
            ctx.moveTo(3, 0);
            ctx.lineTo(-4, -3);
            ctx.lineTo(-1, 0);
            ctx.lineTo(-4, 3);
            ctx.stroke();
            ctx.fill();


            ctx.restore();

            if (DEBUG_LEVEL > 1) {
                ctx.fillText(("h: "+this.current_hit_points).substring(0, 8), this.x + 5, this.y - 5);
                ctx.fillText(("o: "+this.orientation).substring(0, 8), this.x + 5, this.y + 5);
                ctx.fillText(("t: "+this.target_angle).substring(0, 8), this.x + 5, this.y + 15);
            }
        },
        'update': function() {
            // Config for auto pilot
            if (this.target === undefined) {
                let target = { i: NaN, uid: NaN, dist2: Number.POSITIVE_INFINITY } // Continue;
                let i = 0;
                this.world.objects.forEach(o => {
                    if (o.player !== this.player) {
                        let dist2 = pow(this.x - o.x, 2) + pow(this.y - o.y, 2);
                        if (dist2 < target.dist2) {
                            // optimize if it becomes a problem
                            target = {
                                i: i,
                                uid: o.uid,
                                dist2: dist2,
                            };
                        }
                    }
                    ++i;
                });
                
                if (isNaN(target.i) === false) {
                    this.target = target;
                }
            } else {
                // has target
                if (this.world.objects[this.target.i] && this.world.objects[this.target.i].uid === this.target.uid) {
                    this.autopilot_target_id = this.target.i;
                } else {
                    // target died
                    this.target = undefined;
                    // return to mothership
                    this.autopilot_target_id = this.player;
                }
            }

            // TODO Autopilot Movement
            this.tail.push({ x: this.x, y: this.y });
            const x = this.x = this.x_next;
            const y = this.y = this.y_next;
            this.x_next = this.x + cos(this.orientation) * this.gas;
            this.y_next = this.y + sin(this.orientation) * this.gas;

            if (this.autopilot_target_id === this.player) {
                // go home
                this.gas = random();
            } else {
                // hunt
                this.gas = 1.0;
            }

            // Math Magic so I know wether to turn left or right
            const target_x = this.world.objects[this.autopilot_target_id].x_next
                , target_y = this.world.objects[this.autopilot_target_id].y_next;
            var target_angle = atan2(target_y - this.y, target_x - this.x);
            if (target_angle < 0) {
                target_angle += PI2;
            }
            if (target_angle > PI2) {
                target_angle -= PI2;
            }
            this.target_angle = target_angle;
            //const delta_angle = (target_angle - this.orientation) % PI2;
            if (
                // delta_angle > PI
                this.orientation > target_angle
            ) {
                this.orientation -= 0.05;
            } else {
                this.orientation += 0.05;
            }
            
            if (this.orientation > PI2) {
                this.orientation -= PI2;
            }
            if (this.orientation < 0) {
                this.orientation += PI2;
            }
        },
        'get_damage': function() {
            return 5;
        },
    };
}()
