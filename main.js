"use strict"
/// All the player state not needed by the game

+function() {
    var resize_next_frame = true;
    var game = undefined;
    var player_config = [ ];
    var show_remote_dialog = false;
    var ui_state = {};

    window.addEventListener("resize", _ => resize_next_frame = true);

    window.addEventListener("load", _ => {
        console.log("loaded");
        setup_ui();
    });

    console.log("init done");

    function setup_ui() {
        ui_state.screens = document.querySelectorAll('screen');

        activate('game_setup');

        const form = document.querySelector('#game_setup form');

        let html = '';
        for (let i = 0; i < MAX_PLAYERS; ++i) {
            let bg_color = PLAYER_COLORS[i] || '#333';
            html += `
                <div data-player-id="${i}" style="background-color: ${bg_color}">
                    <button>local</button>
                    <button>cpu</button>
                    <button>remote</button>
                </div>
            `;
        }
        let player_setups = form.querySelector('#player_setups');
        player_setups.innerHTML = html;

        // update player_config
        const remote_config_dialog = form.querySelector('#multiplayer_config');
        player_setups.querySelectorAll('button').forEach(bt => {
            const div = bt.parentElement;
            const player_id = parseInt(div.dataset.playerId);

            bt.addEventListener('click', ev => {
                ev.preventDefault();

                if (bt.classList.contains('active')) {
                    // disable this slot
                    player_config[player_id] = undefined;
                    activate_el_in_list(form, div.children);
                } else {
                    // add or change mode
                    player_config[player_id] = bt.innerHTML.trim();
                    activate_el_in_list(bt, div.children);
                }

                show_remote_dialog = player_config.reduce((b, c) => { return b || c === 'remote' }, false);

                if (show_remote_dialog) {
                    Multiplayer.update_player_config(player_config);
                    remote_config_dialog.classList.remove('hidden');
                } else {
                    Multiplayer.update_player_config([]);
                    remote_config_dialog.classList.add('hidden');
                }
            })
        });

        // start game
        form.addEventListener('submit', ev => {
            ev.preventDefault();
            const n_players = player_config.reduce((b, p) => {
                switch (p) {
                    case 'local':
                    case 'cpu':
                    case 'remote':
                        b += 1;
                    default:
                }
                return b
            }, 0);

            if (n_players >= 2 && n_players <= MAX_PLAYERS) {
                activate('active_game');
                setup_new_game(n_players);
            } else {
                alert('choose between 2 and `${MAX_PLAYERS}`');
            }
        });

        document.querySelector('#restart_button').addEventListener('click', _ => activate('game_setup'))
    }
    function activate(id) {
        ui_state.screens.forEach(el => {
            if (el.id === id) {
                el.classList.add('active');
            } else {
                el.classList.remove('active');
            }
        })
    }
    function activate_el_in_list(next, list) {
        for (let i = 0; i < list.length; ++i) {
            const el = list[i];
            if (el === next) {
                el.classList.add('active');
            } else {
                el.classList.remove('active');
            }
        }
    }

    function setup_new_game(n_players) {
        let canvas = document.querySelector("canvas#battleground");
        game = new Game(canvas.getContext('2d'), n_players);
        game.on_game_over = _ev => {
            console.log("on_game_over")
            activate('stats_screen');
            game = undefined;
        }

        // create player buttons
        let html = '';
        for (var i = 0; i < n_players; ++i) {
            let bg_color = PLAYER_COLORS[i] || '#333';
            html += `<div style="border-color: ${bg_color};">
                <div>
                    <button>local</button>
                    <button>cpu</button>
                    <button>remote</button>
                </div>
                <button id="p${i}" style="background-color: ${bg_color};" class="primary_player_button">${i}</button>
            </div>`;
        }
        let buttons = document.querySelector('#buttons');
        buttons.innerHTML = html;

        // Add event handlers
        var i = 0;
        buttons.querySelectorAll(":scope > div")
            .forEach(group => {
                const el = group.querySelector('.primary_player_button');
                el.addEventListener("mousedown", player_button_handler);
                el.addEventListener("mouseup", player_button_handler);

                group.querySelectorAll('div button').forEach(el => {
                    el.addEventListener("click", setup_player_control_button(i));
                    if (el.innerHTML.trim() === player_config[i]) {
                        el.classList.add('active');
                    }
                })
                i += 1;
            })

        requestAnimationFrame(limited_fps_runner);
    }

    var last_frame = 0;
    function limited_fps_runner() {
        let now = Date.now();
        if (resize_next_frame) {
            console.log("resize")
        }
        game.render(resize_next_frame);
        player_config.forEach((kind, player_id) => {
            if (kind === 'cpu') {
                if (random() < 0.05) {
                    const input = new InputStream(player_id, 'do_construction');
                    game.submit_player_input(input);
                }
            }
        })
        //console.log(["frame", now, resize_next_frame]);

        let delta = now - last_frame; // will always be positive
        if (delta > max_fps) {
            // missed deadline
            requestAnimationFrame(limited_fps_runner);
        } else {
            setTimeout(x => requestAnimationFrame(limited_fps_runner), max_fps - delta);
        }
        last_frame = now;
        resize_next_frame = false;
    }

    /// Assumes the Player ID to be at .innerHTML
    function player_button_handler(event) {
        let player = parseInt(this.innerHTML);

        // TODO consider event.type for mousedown/mouseup
        switch (event.type) {
            case 'mousedown':
                var input = new InputStream(player, 'start_construction');
                break;
            case 'mouseup':
                var input = new InputStream(player, 'do_construction');
                break;
            default:
                throw ["unexpected event.type", event.type];
        }

        game.submit_player_input(input);
    }

    function setup_player_control_button(player_id) {
        return function() {
            player_config[player_id] = this.innerHTML;

            this.parentElement.querySelectorAll('*').forEach(x => x.classList.remove('active'))
            this.classList.add('active')
        }
    }
}()
