function RingBuf(size) {
    this.buf = new Array(size);
    this.length = 0;
    this.insert_index = 0;
}

+function() {
    "use strict"
    RingBuf.prototype = {
        push: function(e) {
            this.buf[this.insert_index] = e;
            this.insert_index = (this.insert_index + 1) % this.buf.length;
            if (this.length < this.buf.length) {
                this.length += 1;
            }
        },
        forEach: function(cb) {
            for (let index = 0; index < this.length - 1; ++index) {
                const element = this.buf[(index + this.insert_index + 1) % this.length];
                cb(element);
            }
        },
        first: function() {
            if (this.length === 0) {
                return undefined;
            } else {
                return this.buf[(this.insert_index + 1) % this.length];
            }
        },
        reset: function() {
            this.length = 0;
            this.insert_index = 0;
        }
    }
}()